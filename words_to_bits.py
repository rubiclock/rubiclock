import numpy as np
import scipy.signal


def words_to_bits(words):
    # takes an array of 8bit numbers and returns a 
    # 2D array where each line contains the bits of one of the numbers
    return np.unpackbits(
        np.array(words, dtype=np.uint8)
    ).reshape(len(words), 8)


def prepare_sequence_for_spi(words, cycles_between_bytes=0):
    # Bits is an n x 8 array of bits representing the SPI words
    bits = words_to_bits(words)

    spi_sequence = np.zeros((bits.shape[0], 8 + cycles_between_bytes), dtype=np.uint32)
    spi_sequence[:bits.shape[0], :8] = bits
    spi_sequence = spi_sequence.ravel()
    spi_sequence = np.repeat(spi_sequence, repeats=2)

    spi_clk = 0.5*(1+scipy.signal.square(duty=0.5, t=np.pi*(1+np.arange(bits.size*2)) + 0.001))
    spi_clk = spi_clk.astype(np.uint32)
    
    if cycles_between_bytes > 0:
        spi_clk = spi_clk.reshape((bits.shape[0], 16))
        spi_clk = np.hstack((
            spi_clk, 
            np.zeros((bits.shape[0], cycles_between_bytes*2))),
        )
        spi_clk = spi_clk.ravel()
    else:
        spi_sequence = np.concatenate((spi_sequence, np.zeros(1, dtype=np.uint32)))
        spi_clk = np.concatenate((spi_clk, np.zeros(1, dtype=np.uint32)))

    return spi_clk, spi_sequence

# words = np.floor(10*np.random.random(4)).astype(np.uint8)

# # words = [0b01100001, 0b10101011, 0b00110000, 0b01011101, 0b11111101, 0b00110001, 0b01100011, 0b10000010]

# spi_clk, spi_sequence = prepare_sequence_for_spi(words, cycles_between_bytes=2)
# spi_length = len(spi_clk)

# fig, ax = plt.subplots(figsize=(20, 2))
# ax.bar(np.arange(spi_length), spi_clk, width=1.0, color="None", edgecolor="blue", linewidth=2.0)
# ax.bar(np.arange(spi_length), 0.5*spi_sequence, width=1.0, color="orange")
# ax.set_ylim(-0.1, 1.1)
# ax.set_xlim(0, 18)
# ax.grid()

if __name__ == '__main__':
    from ttlplot import ttl_plot
    words = [0xff, 0x0f, 0xf0, 0xa6]
    spi_clk, spi_sequence = prepare_sequence_for_spi(words, cycles_between_bytes=1)
    words = np.zeros(spi_clk.size, dtype=np.uint8)
    words |=  (spi_sequence << 1) | (spi_clk << 0)
    ttl_plot(words=words, bits=(0, 1))