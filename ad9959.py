import enum
import numpy as np
import math

from wordbuffer import WordBuffer


def split_to_bytes(full_word, n_bytes):
    words = []
    for _ in range(n_bytes):
        words.append(full_word & 0xff)
        full_word >>= 8
    return words[::-1]


class AD9959:
    class CHANNEL(enum.IntFlag):
        CH0 = 0b0001
        CH1 = 0b0010
        CH2 = 0b0100
        CH3 = 0b1000
        ALL = 0b1111
        
    REGISTER = enum.IntEnum('REGISTER', 'CSR FR1 FR2 CFR CFTW0 CPOW0 ACR LSRR RDW FDW', start=0)
    SERIAL_IO_MODE = enum.IntEnum('SERIAL_IO_MODE', 'TWO_WIRE THREE_WIRE TWO_BIT_SERIAL FOUR_BIT_SERIAL', start=0)
    VCO_GAIN_CONTROL = enum.IntEnum('VCO_GAIN_CONTROL', 'LOW HIGH', start=0)
    CHARGE_PUMP_CONTROL = enum.IntEnum('CHARGE_PUMP_CONTROL', 'CPC75UA CPC100UA CPC125UA CPC150UA', start=0)
    
    REGISTER_SIZES_BYTES = {
        REGISTER.CSR:   1,
        REGISTER.FR1:   3,
        REGISTER.FR2:   2,
        REGISTER.CFR:   3,
        REGISTER.CFTW0: 4,
        REGISTER.CPOW0: 2,
        REGISTER.ACR:   3,
        REGISTER.LSRR:  2,
        REGISTER.RDW:   4,
        REGISTER.FDW:   4,
    }
    
    """
    All these REG_BITS were taken from https://github.com/cjheath/AD9959
    A C++ driver for the AD9959
    """
    
    class INSTR_BITS(enum.IntEnum):
        WRITE = 0x00
        READ  = 0x80
    
    class CSR_BITS(enum.IntEnum):
        MSB_First = 0x00
        LSB_First = 0x01
        # Serial I/O Modes (default IO2Wire):
        IO2Wire = 0x00
        IO3Wire = 0x02
        IO2Bit  = 0x04
        IO4Bit  = 0x06

    class FR1_BITS(enum.IntEnum):    # Function Register 1 is 3 bytes wide.
        # Most significant byte:
        # Higher charge pump values decrease lock time and increase phase noise
        ChargePump0      = 0x000000
        ChargePump1      = 0x010000
        ChargePump2      = 0x020000
        ChargePump3      = 0x030000

        PllDivider       = 0x040000 # multiply 4..20 by this (or shift 19)
        VCOGain          = 0x800000 # Set low for VCO<160MHz, high for >255MHz

        # Middle byte:
        ModLevels2       = 0x000000 # How many levels of modulation?
        ModLevels4       = 0x000100
        ModLevels8       = 0x000200
        ModLevels16      = 0x000300

        RampUpDownOff    = 0x000000
        RampUpDownP2P3   = 0x000400 # Profile=0 means ramp-up, 1 means ramp-down
        RampUpDownP3     = 0x000800 # Profile=0 means ramp-up, 1 means ramp-down
        RampUpDownSDIO123= 0x000C00 # Only in 1-bit I/O mode

        Profile0         = 0x000000
        Profile7         = 0x000700

        # Least significant byte:
        SyncAuto         = 0x000000 # Master SYNC_OUT->Slave SYNC_IN, with FR2
        SyncSoft         = 0x000001 # Each time this is set, system clock slips one cycle
        SyncHard         = 0x000002 # Synchronise devices by slipping on SYNC_IN signal

        # Software can power-down individual channels (using CFR[7:6])
        DACRefPwrDown    = 0x10 # Power-down DAC reference
        SyncClkDisable   = 0x20 # Don't output SYNC_CLK
        ExtFullPwrDown   = 0x40 # External power-down means full power-down (DAC&PLL)
        RefClkInPwrDown  = 0x80 # Disable reference clock input

    class FR2_BITS(enum.IntEnum):
        AllChanAutoClearSweep    = 0x8000  # Clear sweep accumulator(s) on I/O_UPDATE
        AllChanClearSweep        = 0x4000  # Clear sweep accumulator(s) immediately
        AllChanAutoClearPhase    = 0x2000  # Clear phase accumulator(s) on I/O_UPDATE
        AllChanClearPhase        = 0x2000  # Clear phase accumulator(s) immediately
        AutoSyncEnable    = 0x0080
        MasterSyncEnable  = 0x0040
        MasterSyncStatus  = 0x0020
        MasterSyncMask    = 0x0010
        SystemClockOffset = 0x0003      # Mask for 2-bit clock offset controls

    class CFR_BITS(enum.IntEnum):
        ModulationMode     = 0xC00000     # Mask for modulation mode
        AmplitudeModulation= 0x400000     # Mask for modulation mode
        FrequencyModulation= 0x800000     # Mask for modulation mode
        PhaseModulation    = 0xC00000     # Mask for modulation mode
        SweepNoDwell       = 0x008000     # No dwell mode
        SweepEnable        = 0x004000     # Enable the sweep
        SweepStepTimerExt  = 0x002000     # Reset the sweep step timer on I/O_UPDATE
        DACFullScale       = 0x000300     # 1/8 1/4 1/2 or full DAC current
        DigitalPowerDown   = 0x000080     # Power down the DDS core
        DACPowerDown       = 0x000040     # Power down the DAC
        MatchPipeDelay     = 0x000020     # Compensate for pipeline delays
        AutoclearSweep     = 0x000010     # Clear the sweep accumulator on I/O_UPDATE
        ClearSweep         = 0x000008     # Clear the sweep accumulator immediately
        AutoclearPhase     = 0x000004     # Clear the phase accumulator on I/O_UPDATE
        ClearPhase         = 0x000002     # Clear the phase accumulator immediately
        OutputSineWave     = 0x000001     # default is cosine
    
    def __init__(self, ref_clock, pll_divider=1, LSB_first=None, serial_io_mode=None):
        self.ref_clock = ref_clock
        self.pll_divider = pll_divider
        self.LSB_first = AD9959.CSR_BITS.MSB_First if LSB_first is None else LSB_first
        self.serial_io_mode = AD9959.CSR_BITS.IO3Wire if serial_io_mode is None else serial_io_mode
    
    @property
    def system_clock(self):
        return self.ref_clock * self.pll_divider
    
    def get_instruction_word(self, register_to_write, write=True):
        word = 0
        if not write:
            word |= AD9959.INSTR_BITS.READ
        word |= register_to_write
        return word
    
    def get_CSR_word(self, channels):
        word = 0
        word |= channels << 4
        word |= self.serial_io_mode
        word |= self.LSB_first
        return word
            
    def get_FR1_word(self):
        word = 0
        if self.system_clock > 255e6:
            word |= AD9959.VCO_GAIN_CONTROL.HIGH << 23
        
        if 4 <= self.pll_divider <= 20:
            word |= self.pll_divider << 18 
        return word
    
    def get_CFR_word(self):
        word = 0
        word |= 0x03 << 8
        return word
    
    def get_CFTW0_word(self, frequency):
        FTW = round(frequency * 2**32/self.system_clock)
        return FTW & 0xffffffff
        
    def get_CPOW0_word(self, phase, rad=False):
        if rad:
            phase = phase*180/math.pi

        POW = round(phase * 2**14 / 360.0)
        return POW & 0b0011111111111111
    
    def get_set_single_frequency_words(self, channels, frequency, phase=0):
        buffer = WordBuffer()
        
        buffer.add_word(AD9959.INSTR_BITS.WRITE | AD9959.REGISTER.CSR)
        buffer.add_word(
            AD9959.CSR_BITS.IO3Wire | AD9959.CSR_BITS.MSB_First | (channels << 4), 
            n_bytes=AD9959.REGISTER_SIZES_BYTES[AD9959.REGISTER.CSR],
        )
        
        buffer.add_word(AD9959.INSTR_BITS.WRITE | AD9959.REGISTER.FR1)
        buffer.add_word(
            AD9959.FR1_BITS.VCOGain | AD9959.FR1_BITS.PllDivider*self.pll_divider, 
            n_bytes=AD9959.REGISTER_SIZES_BYTES[AD9959.REGISTER.FR1],
        )
        buffer.add_word(AD9959.INSTR_BITS.WRITE | AD9959.REGISTER.CFR)
        buffer.add_word(
            AD9959.CFR_BITS.DACFullScale, 
            n_bytes=AD9959.REGISTER_SIZES_BYTES[AD9959.REGISTER.CFR],
        )
        buffer.add_word(AD9959.INSTR_BITS.WRITE | AD9959.REGISTER.CFTW0)
        buffer.add_word(
            self.get_CFTW0_word(frequency=frequency),
            n_bytes=AD9959.REGISTER_SIZES_BYTES[AD9959.REGISTER.CFTW0],
        )
        buffer.add_word(AD9959.INSTR_BITS.WRITE | AD9959.REGISTER.CPOW0)
        buffer.add_word(
            self.get_CPOW0_word(phase=phase),
            n_bytes=AD9959.REGISTER_SIZES_BYTES[AD9959.REGISTER.CPOW0],
        )
        
        return buffer

    def get_read_words(self, register):
        if register not in AD9959.REGISTER:
            raise ValueError('{} is not a valid register for the AD9959'.format(register))
        
        buffer = WordBuffer()
        buffer.add_word(AD9959.INSTR_BITS.READ | register)
        buffer.add_word(0, n_bytes=AD9959.REGISTER_SIZES_BYTES[register])
        
        return buffer
        
    def get_set_serial_mode_words(self):
        buffer = WordBuffer()
        buffer.add_word(AD9959.INSTR_BITS.WRITE | AD9959.REGISTER.CSR)
        buffer.add_word((AD9959.CHANNEL.ALL <<4) | self.serial_io_mode | self.LSB_first)
  
        return buffer    

if __name__ == '__main__':
    
    dds = AD9959(ref_clock=100e6, pll_divider=5)
    buffer = dds.get_set_single_frequency_words(channels=AD9959.CHANNEL.CH0,frequency=100e6, phase=0)
    print(buffer)