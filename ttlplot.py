import matplotlib.pyplot as plt
import numpy as np

def ttl_plot(words, bits, labels=None, fig_kwargs=None):
    """
        param: words: list of integers with each bit representing one TTL signal
        param: bits: list of bits to plot
        param: label: labels of the bits
        param: fig_kwargs: dict of figure parameters to pass to pyplot.subplots

        return: fig, ax
    """
    fig_kwargs = (
        dict() if fig_kwargs is None 
        else fig_kwargs
    )
    fig, ax = plt.subplots(**fig_kwargs)
    
    labels = [] if labels is None else labels

    # Generate the time values
    t = np.repeat(np.arange(len(words)), repeats=2)
    t[:-1] = t[1:]
    t[-1] = len(words)

    # Duplicating every word to plot squares
    words = np.repeat(np.array(words), repeats=2)

    # Adding a zero at the begining and end
#     words = np.concatenate(([0], words, [0]))
#     t = np.concatenate(([0], t, [len(words)//2-1]))

    for num, bit in enumerate(bits):
        # Plot each bit "line" with a vertical offset
        ax.plot(
            t, ((words & (1 << bit)) >> bit) + num*1.1,
            linewidth=3.0
        )
        # Try to add labels if there are any
        try:
            label = labels[num]
        except IndexError:
            pass
        else:
            ax.text(
                x=-t[-1]/100,y=num*1.1+0.55,s=label, 
                rotation=90, 
                verticalalignment='center', horizontalalignment='center',
            )
        
#     ax.set_xlim(-t[-1]/10, t[-1]*1.1)
    ax.set_ylim(-0.2, len(bits)*1.1+0.2)

    # Set yticks and horizontal gridlines for each bit "line"
    ax.set_yticks(np.arange(len(bits))*1.1)
    ax.set_yticklabels([])
    ax.grid(which='major', axis='y', linewidth=1.0, color='black')
    return fig, ax
        
if __name__ == '__main__':
    fig, ax = ttl_plot(
        words=[0b01, 0b10, 0b11, 0b10], 
        bits=(0, 1, 2), 
        labels=['1010', '0111', '0000'],
    )
    
    ax.set_xticks(np.arange(4+1))
    ax.grid(which='major', linewidth=1.0, axis='both')

    plt.show()