import warnings


class WordBuffer(list):    
    def add_word(self, word, n_bytes=1, lsb_first=False):
        if n_bytes == 1:
            if word > 255:
                warnings.warn('0x{:x} added to buffer is more than one byte, it will be truncated.'.format(word), RuntimeWarning)
            self.append(word & 0xff)
        else:
            words_to_add = []
            for _ in range(n_bytes):
                words_to_add.append(word & 0xff)
                word >>= 8
            self.extend(words_to_add[::-1])
    
    def add_words(self, words):
        self.extend(words)

    def __add__(self, rbuffer):
        return WordBuffer(super().__add__(rbuffer))
            
    def __radd__(self, lbuffer):
        return WordBuffer(lbuffer + list(self))        
    
    def __repr__(self):
        return 'WordBuffer(words={})'.format(list(self).__repr__())
    
    def __str__(self):
        return ':'.join(('{:02x}'.format(w) for w in self))    
            

if __name__ == '__main__':
    buffer = WordBuffer([0x01, 0x02])
    assert(buffer == [0x01, 0x02])
    buffer = WordBuffer(buffer)
    assert(buffer == [0x01, 0x02])
    buffer.add_word(0xff03)
    assert(buffer == [0x01, 0x02, 0x03])
    buffer.add_word(0x0605, n_bytes=2)
    assert(buffer == [0x01, 0x02, 0x03, 0x06, 0x05])
    buffer.add_words([0x07, 0x08])
    assert(buffer == [0x01, 0x02, 0x03, 0x06, 0x05, 0x07, 0x08])
    buffer = buffer + [0x09, 0x0a]
    assert(buffer == [0x01, 0x02, 0x03, 0x06, 0x05, 0x07, 0x08, 0x09, 0x0a])
    buffer = [0x00] + buffer
    assert(buffer == [0x00, 0x01, 0x02, 0x03, 0x06, 0x05, 0x07, 0x08, 0x09, 0x0a])
    
    try:
        buffer + 1
    except TypeError as e:
        print(e)
        
    buffer[0] = 1
    assert(buffer == [0x01, 0x01, 0x02, 0x03, 0x06, 0x05, 0x07, 0x08, 0x09, 0x0a])
    
    print('Buffer content :', buffer)
    
    