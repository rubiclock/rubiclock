import enum

from wordbuffer import WordBuffer


def split_to_bytes(full_word, n_bytes):
    words = []
    for _ in range(n_bytes):
        words.append(full_word & 0xff)
        full_word >>= 8
    return words[::-1]


class AD9912():
        
    class REGISTER(enum.IntEnum):
        # MSB address
        SERIAL_CFG = 0x0000
        FTW0       = 0x01AB
        IO_UPDATE  = 0x0005

    class INSTR_BITS(enum.IntEnum):
        READ   = 0x8000
        WRITE  = 0x0000
        ONE_BYTE    = 0x0000
        TWO_BYTES   = 0x2000
        THREE_BYTES = 0x4000
        STREAM      = 0x6000
        
    class SERIAL_CFG_BITS(enum.IntEnum):
        SDO_ACTIVE = 0x81
        LSB_FIRST  = 0x42
        SOFT_RESET = 0x24
        LONG_INSTR = 0x18
         
    def __init__(self, ref_clock):
        self.ref_clock = ref_clock
    
    def get_FTW_word(self, frequency):
        FTW = round(2**48*frequency/self.ref_clock)
        return FTW        
    
    def get_set_frequency_words(self, frequency):
        buffer = WordBuffer()
        buffer.add_word(
            AD9912.INSTR_BITS.WRITE | AD9912.INSTR_BITS.THREE_BYTES | AD9912.REGISTER.FTW0, 
            n_bytes=2,
        )
        FTW = self.get_FTW_word(frequency=frequency)
        buffer.add_word((FTW & 0xFFFFFF000000) >> 24, n_bytes=3)
        buffer.add_word(
            AD9912.INSTR_BITS.WRITE | AD9912.INSTR_BITS.THREE_BYTES | (AD9912.REGISTER.FTW0 - 3),
            n_bytes=2,
        )
        buffer.add_word(FTW & 0x000000ffffff, n_bytes=3)
        
        return buffer 
            
    def get_io_update_words(self):
        words = []
        
        instr_word = AD9912.INSTR_BITS.WRITE | AD9912.INSTR_BITS.BYTES1 | AD9912.REGISTER.IO_UPDATE
        words.extend([(instr_word & 0xff00) >> 8, instr_word & 0x00ff])
        
        register_word = 0x1
        words.append(register_word)
        
        return words
        
        
if __name__ == '__main__':
    DDS = AD9912(ref_clock=875e6)

    words = DDS.get_set_frequency_words(165.317389e6)
    print(words)
    print(WordBuffer([0b01100001, 0b10101011, 0b00110000, 0b01011101, 0b11111101, 0b00110001, 0b01100011, 0b10000010]))
    
#     for word in [0b01100001, 0b10101011, 0b00110000, 0b01011101, 0b11111101, 0b00110001, 0b01100011, 0b10000010]:
#         print('0x{:02x}'.format(word))